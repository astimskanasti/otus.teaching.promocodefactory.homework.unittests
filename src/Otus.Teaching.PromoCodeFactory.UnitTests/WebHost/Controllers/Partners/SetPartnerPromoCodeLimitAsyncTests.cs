﻿using System;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly PartnersController _partnersController;
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        
        /// <summary>
        /// Если партнер не найден, выдается ошибка 404
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            //Arrange
            var autoFixture = new Fixture();
            var partnerId = Guid.NewGuid();
            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();     
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId))
                .ReturnsAsync((Partner)null);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, выдается ошибка 400
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithActive(false)
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_NumberIssuedPromoCodesShouldBeZero()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithActiveLimit()
                .Create();
            
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);
            
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется лимит,
        /// но прошлый еще не закончился, то количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerPreviousLimitIsEnd_NumberIssuedPromoCodesShouldNotBeZero()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNumberIssuedPromoCodes(100)
                .WithNotActiveLimit()
                .Create();
            
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);
            
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        /// <summary>
        /// При установке лимита отключается предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerHasActiveLimit_CancelPreviousLimit()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>().Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .WithNotActiveLimit()
                .Create();
            
            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);
            
            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            partner.PartnerLimits.First().CancelDate.Should().HaveValue();
        }
        
        /// <summary>
        /// Лимит должен быть больше 0
        /// </summary>
        [Theory]
        [InlineData(0)]
        [InlineData(-100)]
        public async void SetPartnerPromoCodeLimitAsync_LimitLessOrEqualsZero_ReturnsBadRequest(int limit)
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, limit)
                .Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }
        
        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_LimitIsValid_SaveSuccessfully()
        {
            //Arrange
            var autoFixture = new Fixture();
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());

            var partnerPromoCodeLimitRequest = autoFixture.Build<SetPartnerPromoCodeLimitRequest>()
                .With(x => x.Limit, 10)
                .Create();

            var partner = new PartnerBuilder()
                .WithBaseProperties()
                .Create();

            _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id))
                .ReturnsAsync(partner);

            //Act
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, partnerPromoCodeLimitRequest);

            //Assert
            _partnersRepositoryMock.Verify(repo => repo.UpdateAsync(partner), Times.Once);
        }
    }
}